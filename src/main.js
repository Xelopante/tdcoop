import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import bulma from 'bulma'

Vue.prototype.$api = axios.create({
  baseURL: 'https://tools.sopress.net/iut/coop/api/',
  headers: { Authorization: '366f1e9158f8bac2be489fe0558573c5701e8747' }
});

//https://tools.sopress.net/iut/coop/doc/

Vue.prototype.$api.interceptors.request.use((config) => {
  config.params = config.params || {};
  config.params.token = store.state.token;
  return config;
});


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
