import Vue from 'vue'
import VueRouter from 'vue-router'
import Connexion from '../views/Connexion.vue'
import CreateCompte from '../views/CreateCompte.vue'
import Accueil from '../views/Accueil'
import Conversation from '../views/Conversation'
import Membres from '../views/Membres'
import Membre from '../views/Membre'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Accueil',
    component: Accueil
  },
  {
    path: '/conversation/:id',
    name: 'Conversation',
    component: Conversation
  },
  {
    path: '/creation-compte',
    name: 'Creation-compte',
    component: CreateCompte
  },
  {
    path: '/connexion',
    name: 'Connexion',
    component: Connexion
  },
  {
    path: '/membres',
    name: 'Membres',
    component: Membres
  },
  {
    path: '/membre/:id',
    name: 'Membre',
    component: Membre
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
